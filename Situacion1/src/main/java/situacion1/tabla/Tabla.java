package situacion1.tabla;

import situacion1.clientes.Cliente;
import situacion1.administracion.EstudioLegal;
import situacion1.excepciones.ClienteInexistenteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Usuario
 */
public class Tabla extends DefaultTableModel {
    String columnas[];
    static Integer NOMBRE=0; 
    static Integer APELLIDO=1;
    static Integer DNI=2;
    private EstudioLegal estudio;
    
    public Tabla(String columnas[]) {
        super();
        this.columnas = columnas;
        this.setColumnIdentifiers(columnas);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if (column == 3) {
            return true;
        } else {
            return false;
        }
    }

    public void cargarDatoAlaTabla(EstudioLegal estudioLegal) {
        this.estudio = estudioLegal;
        Object fila[] = new Object[3];
        for(int i =0; i<estudioLegal.getClientes().size();i++){
            fila[NOMBRE]=estudioLegal.getClientes().get(i).getNombre();
            fila[APELLIDO]=estudioLegal.getClientes().get(i).getApellido();
            fila[DNI]=estudioLegal.getClientes().get(i).getDni();
        }
        this.addRow(fila);
    }

    public void cargarDatos(EstudioLegal estudio){
        Object fila[] = new Object[4];
        for(int i =0; i<estudio.getClientes().size();i++){
            fila[NOMBRE]=estudio.getClientes().get(i).getNombre();
            fila[APELLIDO]=estudio.getClientes().get(i).getApellido();
            fila[DNI]=estudio.getClientes().get(i).getDni();
            this.addRow(fila);
        }
    }


    public void eliminarElemento(JTable tabla,EstudioLegal estudio){
        Integer fila;
        fila = tabla.getSelectedRow();
        String datoDeFila = String.valueOf(this.getValueAt(fila, DNI));
        Integer dni = Integer.parseInt(datoDeFila);
        if(fila>=0){
            try{
                this.removeRow(fila);
                estudio.eliminarCliente(dni);
            } catch (ClienteInexistenteException e) {
                Logger.getLogger(Tabla.class.getName()).log(Level.SEVERE, null, e);
            }
        }else{       
            JOptionPane.showMessageDialog(null,"Por Favor Seleccione un Producto");  
        }               
    }


    public void cargarUnDato(Cliente cliente, JTable tabla){
        tabla.removeAll();
        Object [] fila = new Object [4];
        fila[NOMBRE] =cliente.getNombre();
        fila[APELLIDO]=cliente.getApellido();
        fila[DNI]=cliente.getDni();
        this.addRow(fila);
    }
}

