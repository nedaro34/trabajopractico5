package situacion1.administracion;

import java.util.ArrayList;
import java.util.Collections;
import situacion1.clientes.Cliente;

import situacion1.excepciones.*;
import situacion1.utilidadesOrdenamiento.*;

public class EstudioLegal {
  
    private ArrayList<Cliente> clientes;
	
    public EstudioLegal(){
        this.setClientes(new ArrayList<Cliente>());
    }

    public void setClientes(ArrayList<Cliente> clientes){
        this.clientes = clientes;
    }

    public ArrayList<Cliente> getClientes(){
        return clientes;
    }
    
    public void agregarCliente(Cliente cliente) throws ClienteYaExisteException {
        for (Cliente var : clientes) {
            if (var.getDni().equals(cliente.getDni())){
                throw new ClienteYaExisteException();
            }
        }
        clientes.add(cliente);
    }

    public void eliminarCliente(Integer dni) throws ClienteInexistenteException {
        Cliente clienteEncontrado = getCliente(dni);
        clientes.remove(clienteEncontrado);
    }

    public Cliente getCliente(Integer dni) throws ClienteInexistenteException{
        Cliente clienteEncontrado = null;
        for (Cliente var : clientes){
            if(var.getDni().equals(dni)){
                clienteEncontrado = var;
            }
        }
        if (clienteEncontrado == null) {
            throw new ClienteInexistenteException();
        }
        return clienteEncontrado; 
    }
    
    public void modificarCliente(Cliente nuevoCliente) throws ClienteInexistenteException {
       Cliente clienteEncontrado = null;
       clienteEncontrado = getCliente(nuevoCliente.getDni());
       clientes.remove(clienteEncontrado);
       clientes.add(nuevoCliente);
    }  
    
    public void OrdenarPorApellido(){
        Collections.sort(clientes, new OrdenarApellido());             
    }

    public void OrdenarPorDni(){
        Collections.sort(clientes, new OrdenarDni());
    }
}
    