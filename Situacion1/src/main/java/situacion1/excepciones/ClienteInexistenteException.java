package situacion1.excepciones;

@SuppressWarnings("serial")
public class ClienteInexistenteException extends Exception{
    public ClienteInexistenteException(){
        super("El cliente no existe");
    }
}