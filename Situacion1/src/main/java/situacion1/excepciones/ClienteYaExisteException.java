package situacion1.excepciones;

@SuppressWarnings("serial")
public class ClienteYaExisteException extends Exception{
    public ClienteYaExisteException(){
        super("El Cliente ya existe");
    }
}