package situacion1.clientes;

public class DomicilioBarrial extends Domicilio {

 private String nombre;
 private String manzana;
 private Integer numeroCasa;

    public DomicilioBarrial(String nombre, String manzana, Integer numeroCasa) {
        this.nombre=nombre;
        this.manzana=manzana;
        this.numeroCasa=numeroCasa;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String nombre() {
        return nombre;
    }
    
    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    public String getManzana(){
        return manzana;
    }
    
    public void setNumeroCasa(Integer numeroCasa) {
        this.numeroCasa = numeroCasa;
    }

    public Integer getNumeroCasa() {
        return numeroCasa;
    }
    
    @Override
    public String mostrarInformacion() {
        return "NOMBRE : " + nombre +"\n"+ "MANZANA : " + manzana +"\n"+"NUMERO DE CASA : " + numeroCasa;
    }
}