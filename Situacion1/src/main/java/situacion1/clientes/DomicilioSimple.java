package situacion1.clientes;

public  class DomicilioSimple extends Domicilio{
    
    private String calle;
    private Integer numero;

    public DomicilioSimple(String calle, Integer numero){
        this.calle=calle;
        this.numero=numero;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }
    
    public String getCalle() {
        return calle;
    }
    
    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getNumero() {
        return numero;
    }
    
    @Override
    public String mostrarInformacion() {
        return "CALLE : "+ calle +"\n" +"NUMERO :" + numero;
    }
}