package situacion1.clientes;

public class DomicilioEdificio extends Domicilio{

    private String calle;
    private Integer  piso;
    private String departamento;

    public DomicilioEdificio(String calle, Integer piso,String departamento) {
        this.calle=calle;
        this.piso=piso;
        this.departamento=departamento;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCalle(){
        return calle;
    }
    
    public void setPiso(Integer piso) {
        this.piso = piso;
    }
    
    public Integer getPiso() {
        return piso;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDeparmento() {
        return departamento;
    }
    
    @Override
    public String mostrarInformacion() {
        return "CALLE : " + calle +"\n"+ "PISO : " + piso +"\n"+"DEPARTAMENTO : " + departamento;
    }
}