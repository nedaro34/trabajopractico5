package situacion1.clientes;

public class Cliente implements Comparable<Cliente> {
    
    private String nombre;
    private String apellido;
    private Integer dni;
    private Domicilio domicilio;
    private String correoElectronico;
    private String asunto;
    private String numeroTelefono;

    public Cliente(String nombre,String apellido,Integer dni , Domicilio domicilio ,String correoElectronico,String asunto, String numeroTelefono){
        this.nombre=nombre;
        this.apellido=apellido;
        this.dni=dni;
        this.domicilio=domicilio;
        this.correoElectronico=correoElectronico;
        this.asunto=asunto;
        this.numeroTelefono=numeroTelefono;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
    
    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public Integer getDni() {
        return dni;
    }
    
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    public String getApellido() {
        return apellido;
    }
    
    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }
    
    public Domicilio getDomicilio() {
        return domicilio;
    }
    
    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }
    
    public String getCorreoElectronico() {
        return correoElectronico;
    }
    
    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getAsunto() {
        return asunto;
    }
    
    public void setNumeroTelefonico(String numeroTelefonico) {
        this.numeroTelefono = numeroTelefonico;
    }
    
    public String numeroTelefonico(){
        return numeroTelefono;
    }
    
    @Override 
    public String toString(){
        return "Nombre: "+nombre+"\n"+"Apellido: "+apellido+"\n"+"Dni :"+dni+"\n"+"Correo Electronico: "+correoElectronico+"\n"+"Numero Telefonico :"+numeroTelefono+"\n"+"Asunto: "+asunto;
    }

    @Override
    public int compareTo(Cliente o){
        return  dni.compareTo(o.getDni());
    }
}

