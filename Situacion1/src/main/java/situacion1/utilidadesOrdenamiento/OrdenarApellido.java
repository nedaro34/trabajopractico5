
package situacion1.utilidadesOrdenamiento;

import java.util.Comparator;
import situacion1.clientes.Cliente;

/**
 *
 * @author Usuario
 */
public class OrdenarApellido implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
       
        Cliente cliente1 =(Cliente)o1; 
        Cliente cliente2  =(Cliente)o2;
        return cliente1.getApellido().compareTo(cliente2.getApellido());
    }
    
    
    
    
}
