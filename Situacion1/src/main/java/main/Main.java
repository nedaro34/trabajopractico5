package main;

import situacion1.administracion.EstudioLegal;
import situacion1.gui.MenuPrincipal;

public class Main {

    public static void main(String[] args){
        EstudioLegal estudio = new EstudioLegal();
        MenuPrincipal menuPrincipal = new MenuPrincipal(estudio);
        menuPrincipal.setVisible(true);
    }
}