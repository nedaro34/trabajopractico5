package situacion2;

import situacion2.excepciones.*;
import situacion2.items.*;
import situacion2.ordenes.*;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

public class OrdenVentaTest {

    private BigDecimal precio;
    private BigDecimal cantidad;
    Item pala;
    Item sierra;

    @Before
    public void setUp() {
        precio = new BigDecimal("500.00");
        cantidad = new BigDecimal("6");
        pala = new HerramientaManual("pala", 4535, precio, cantidad);
        sierra = new HerramientaManual("sierra", 623, precio, cantidad);
    }


    @Test
    public void ComprobarOrderNueva() {
        Integer resultadoEsperado =0;
        OrdenVenta ordenVenta = new OrdenVenta();
        assertEquals(resultadoEsperado, ordenVenta.getsize());
    }

    @Test
    public void ComprobarProductoAlmacenados() throws ProductoExistenteException {
        Integer cantidad = 2;
        OrdenVenta ordenVenta = new OrdenVenta();
        ordenVenta.agregarProducto(pala);
        ordenVenta.agregarProducto(sierra);
        assertEquals(cantidad, ordenVenta.getsize());    
    }
    
    @Test
    public void comprobarMontoTotal() throws ProductoExistenteException {
        BigDecimal ResultadoEsperado = new BigDecimal("6000.00");
        OrdenVenta ordenVenta = new OrdenVenta();
        ordenVenta.agregarProducto(pala);
        ordenVenta.agregarProducto(sierra);
        assertEquals(ResultadoEsperado, ordenVenta.montoTotal());
    }

    @Test (expected = ProductoExistenteException.class)
    public void comprobarProductoRepetido() throws ProductoExistenteException {
        OrdenVenta ordenVenta = new OrdenVenta();
        ordenVenta.agregarProducto(pala);
        ordenVenta.agregarProducto(pala);
    }


    


}
