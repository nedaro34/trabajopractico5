package situacion2.proveedor;

public class Proveedor {
    
    private String nombre;
    private String apellido;
    private Integer dni; 
    private String domicilio;

    public Proveedor(String nombre, String apellido, Integer dni, String domicilio){
        this.nombre=nombre;
        this.apellido=apellido;
        this.dni=dni;
        this.domicilio=domicilio;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getApellido() {
        return apellido;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getDomicilio() {
        return domicilio;
    }

    @Override
    public String toString() {
        return "Apellido: " + apellido +"\nNombre: "+nombre+"\nDNI: " + dni + "\nDomicilio: " + domicilio;
    }
}
