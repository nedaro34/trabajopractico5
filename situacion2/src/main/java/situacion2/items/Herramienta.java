package situacion2.items;

import java.math.BigDecimal;

public abstract class Herramienta implements Item {
    
    private Integer codigo;
    private String nombre;
    private BigDecimal precio;
    private BigDecimal cantidad;

    public Herramienta(String nombre, Integer codigo, BigDecimal precio ,BigDecimal cantidad){
        this.nombre=nombre;
        this.codigo=codigo;
        this.precio=precio;
        this.cantidad=cantidad;
    }

    public void setCodigo(Integer codigo){
        this.codigo = codigo;
    }

    public Integer getCodigo(){
        return codigo;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getNombre(){
        return nombre;
    }
    
    public void setPrecio(BigDecimal precio){
        this.precio = precio;
    }
    
    public BigDecimal getPrecio(){
        return precio;
    }

    public void setCantidad(BigDecimal cantidad){
        this.cantidad = cantidad;
    }
    
    public BigDecimal getCantidad(){
        return cantidad;
    }
}
