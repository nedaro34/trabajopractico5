package situacion2.items;

import java.math.BigDecimal;
import situacion2.proveedor.Proveedor;

public class MateriaPrima {

    private String nombre;
    private BigDecimal precio; 
    private Proveedor proveedor; 

    public MateriaPrima(String nombre, BigDecimal precio, Proveedor proveedor){
        this.nombre=nombre;
        this.precio=precio;
        this.proveedor=proveedor;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getNombre(){
        return nombre;
    }

    public void setPrecio(BigDecimal precio){
        this.precio = precio;
    }

    public BigDecimal getPrecio(){
        return precio;
    }

    public void setProveedor(Proveedor proveedor){
        this.proveedor = proveedor;
    }

    public Proveedor getProveedor(){
        return proveedor;
    }
}
