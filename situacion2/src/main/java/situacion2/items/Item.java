package situacion2.items;


import java.math.BigDecimal;

public interface Item {
    
    public String getNombreProducto();
    public Integer codigoProducto();
    public BigDecimal getprecioUnitario();
    public BigDecimal getPrecioDeVenta();
    public BigDecimal getCantidadArticulos();
}
