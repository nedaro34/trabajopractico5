package situacion2.excepciones;

@SuppressWarnings("serial")
public class ProductoExistenteException extends Exception {
    public ProductoExistenteException(){
        super("El producto ya se encuentra agregado en la orden");
    }
}
