package situacion2.ordenamientos;

import situacion2.items.Item;
import java.util.Comparator;
/**
 *
 * @author Usuario
 */
public class OrdenarPrecio implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Item producto1 =(Item)o1;
        Item producto2 =(Item)o2;
        return producto1.getprecioUnitario().compareTo(producto2.getprecioUnitario());
    }
}
