package main;

import situacion2.ordenes.OrdenVenta;
import situacion2.gui.MenuPrincipal;

public class Main {
   
    public static void main(String[] args) { 
        OrdenVenta ordenVenta = new OrdenVenta();
        MenuPrincipal menuPrincipal = new MenuPrincipal(ordenVenta);
        menuPrincipal.setVisible(true);
    }
}
